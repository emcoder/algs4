use super::{Args, Output};
use std::collections::HashMap;
use std::convert::Into;

type Exercise = (Fn(Args, &mut Output) -> ());

pub struct Exercises {
  exercises: HashMap<String, &'static Exercise>,
}

impl Exercises {
  pub fn new() -> Self {
    Self {
      exercises: HashMap::new(),
    }
  }

  pub fn add<S: Into<String>>(&mut self, name: S, exercise: &'static Exercise) -> &mut Self {
    let name = name.into();
    self.exercises.insert(name, exercise);
    self
  }

  pub fn get<S: Into<String>>(&self, name: S) -> &Exercise {
    let name = name.into();
    let error = format!("Failed to get exercise {}", name);
    self.exercises.get(&name).expect(error.as_str())
  }
}
