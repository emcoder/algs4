use super::*;
use std::env;
use std::ops::Index;
//pub type Args = Vec<String>;

pub struct Args {
  args: Vec<String>,
}

impl Args {
  pub fn new(args: Vec<String>) -> Self {
    Self { args }
  }

  pub fn collect() -> Self {
    let mut args = env::args().collect::<Vec<String>>();
    args.remove(0);
    Self::new(args)
  }

  pub fn remove(&mut self, i: usize) -> String {
    self.args.remove(i)
  }

  pub fn parse<T>(&self, index: usize) -> T
  where
    T: std::str::FromStr,
    <T as std::str::FromStr>::Err: std::fmt::Debug,
  {
    let arg = self
      .args
      .get(index)
      .expect(format!("Missing argument {}", index).as_str());
    let value: Result<T, T::Err> = arg.parse::<T>();
    value.expect(format!("Failed to parse {}!", arg).as_str())
  }

  pub fn get(&self, index: usize) -> &str {
    let err = format!("Missing argument {}", index);
    self.args.get(index).expect(err.as_str()).as_str()
  }

  pub fn float(&self, index: usize) -> Float {
    self.parse::<Float>(index)
  }

  pub fn int(&self, index: usize) -> Int {
    self.parse::<Int>(index)
  }
}

impl Index<usize> for Args {
  type Output = String;

  fn index(&self, index: usize) -> &String {
    self
      .args
      .get(index)
      .expect(format!("Missing argument {}", index).as_str())
  }
}

pub fn args() -> Args {
  Args::collect()
}
