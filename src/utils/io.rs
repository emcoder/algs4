use std::io;

pub type Output = io::Write;
