extern crate algs4;
use algs4::*;

mod exercises;

fn main() {
  let mut args = args();
  let exercise_name = args.remove(0);

  exercises::run(exercise_name, args);
}
