use algs4::*;
use std::io::stdout;

mod template {
  include!("./template.rs");
}

/* 1.1 */
mod ex1_1_3 {
  include!("./1.1/1.1.3.rs");
}
mod ex1_1_5 {
  include!("./1.1/1.1.5.rs");
}

/* 1.3 */
mod ex1_3_4 {
  include!("./1.3/1.3.4.rs");
}

pub fn run(name: String, args: Args) {
  let mut exercises = Exercises::new();

  exercises
    .add("template", &template::main)
    .add("1.1.3", &ex1_1_3::main)
    .add("1.1.5", &ex1_1_5::main)
    .add("1.3.4", &ex1_3_4::main);

  let exercise = exercises.get(name);
  exercise(args, &mut stdout());
}
