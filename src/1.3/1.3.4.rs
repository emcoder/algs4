use algs4::*;
use self::utils::Stack;
mod utils;

pub fn main(args: Args, output: &mut Output) {
  let s = args.get(0);
  let mut stack: Stack<char> = Stack::new();
  for (i, c) in s.chars().enumerate() {
    match c {
      '(' => {
        stack.push(c);
      },
      ')' => {
        if stack.size() == 0 {
          eprintln!("Expression starts with close bracket '{}'!", c);
          outln!(output, "false");
          return;
        }
        let p = stack.pop();
        if p != '(' {
          eprintln!("Invalid bracket after '{}' at position {}: '{}'", p, i + 1, c);
          outln!(output, "false");
          return;
        }
      },
      '{' => {
        stack.push(c);
      },
      '}' => {
        if stack.size() == 0 {
          eprintln!("Expression starts with close bracket '{}'!", c);
          outln!(output, "false");
          return;
        }
        let p = stack.pop();
        if p != '{' {
          eprintln!("Invalid bracket after '{}' at position {}: '{}'", p, i + 1, c);
          outln!(output, "false");
          return;
        }
      },
      '[' => {
        stack.push(c);
      },
      ']' => {
        if stack.size() == 0 {
          eprintln!("Expression starts with close bracket '{}'!", c);
          outln!(output, "false");
          return;
        }
        let p = stack.pop();
        if p != '[' {
          eprintln!("Invalid bracket after '{}' at position {}: '{}'", p, i + 1, c);
          outln!(output, "false");
          return;
        }
      },
      _ => {
        eprintln!("Invalid character '{}' at position {}!", c, i + 1);
        outln!(output, "false");
        return;
      }
    }
  }

  if stack.size() > 0 {
    eprintln!("Unclosed brackets!");
    outln!(output, "false");
    return;
  }

  outln!(output, "true");
}

#[test]
fn test() {
  let args = args!("[()]{}{[()()]()}");
  let output = run_capture!(main, args);
  assert_eq!(output.trim(), "true");

  let args = args!("[(])");
  let output = run_capture!(main, args);
  assert_eq!(output.trim(), "false");
}
