use algs4::*;

type NodeBox<T> = Option<Box<Node<T>>>;

struct Node<T> {
  pub item: T,
  pub next: NodeBox<T>,
}

impl<T> Node<T> {
  pub fn new(item: T) -> Self {
    Self {
      item,
      next: None
    }
  }

  /*pub fn set_next(&mut self, next: Option<&Node>) -> &mut Self {
    self.next = next;
    self
  }*/
}

pub struct Stack<T> {
  first: NodeBox<T>,
  size: Int,
}

impl<T> Stack<T> {
  pub fn new() -> Stack<T> {
    Stack {
      first: None,
      size: 0,
    }
  }

  pub fn push(&mut self, item: T) -> &mut Self {
    let prev_node = self.first.take();
    let mut new_node = Node::new(item);
    new_node.next = prev_node;
    self.first = Some(Box::new(new_node));
    self.size += 1;
    self
  }

  pub fn pop(&mut self) -> T {
    let mut prev_first = self.first.take().expect("Empty stack pop");
    self.first = prev_first.next.take();
    let item: T = prev_first.item;
    self.size -= 1;
    item
  }

  pub fn size(&self) -> Int {
    self.size
  }
}