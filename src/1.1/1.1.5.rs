use algs4::*;

pub fn main(args: Args, output: &mut Output) {
  let (x, y) = (args.float(0), args.float(1));

  if (x > 0.0 && x < 1.0) && (y > 0.0 && y < 1.0) {
    outln!(output, "true");
  } else {
    outln!(output, "false");
  }
}

#[test]
fn test() {
  let args = args!("0.5", "0.99");
  let output = run_capture!(main, args);
  assert_eq!(output.trim(), "true");

  let args = args!("1.9", "0.9");
  let output = run_capture!(main, args);
  assert_eq!(output.trim(), "false");
}
