#[macro_use]
extern crate algs4;

use algs4::*;

fn exercise(args: Args, output: &mut Output) {
  let m: usize = (&args[0]).parse().unwrap();
  let n: usize = (&args[1]).parse().unwrap();

  let mut array: Vec<Vec<usize>> = Vec::new();

  for i in 0..m {
    array.push(Vec::new());
    for j in 0..n {
      array[i].push(i*n + j);
      write!(output, "{} ", array[i][j]).unwrap();
    }
    writeln!(output, "").unwrap();
  }

  writeln!(output, "").unwrap();

  for i in 0..n {
    for j in 0..m {
      write!(output, "{} ", array[j][i]).unwrap();
    }
    writeln!(output, "").unwrap();
  }
}

fn main() {
  run!(exercise);
}

#[cfg(test)]
mod tests {
  use super::exercise;

  #[test]
  fn test() {
    let args = args!("World");
    let output = run_capture!(exercise, args);
    assert_eq!(output.trim(), "Hello, World!");
  }
}
