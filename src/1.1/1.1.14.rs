#[macro_use]
extern crate algs4;

use algs4::*;

fn exercise(args: Args, output: &mut Output) {
  let n: usize = (&args[0]).parse().unwrap();

  let base = 2;
  let mut m = 0;
  let mut i = 1;
  while i*2 <= n {
    i = i * base;
    m += 1;
  }
  writeln!(output, "{}", m).unwrap();
}

fn main() {
  run!(exercise);
}

#[cfg(test)]
mod tests {
  use algs4::*;
  use super::exercise;

  #[test]
  fn test() {
    {
      let args = args!("12");
      let output = run_capture!(exercise, args);
      assert_eq!(output.trim(), "3");
    }
    {
      let args = args!("666");
      let output = run_capture!(exercise, args);
      assert_eq!(output.trim(), "9");
    }
  }
}
