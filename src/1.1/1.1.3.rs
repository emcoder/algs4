use algs4::*;

pub fn main(args: Args, output: &mut Output) {
  let (a, b, c) = (args.int(0), args.int(1), args.int(2));

  if (a == b) && (b == c) {
    outln!(output, "Equal");
  } else {
    outln!(output, "Not equal");
  }
}

#[test]
fn test() {
  let args = args!("1", "2", "3");
  let output = run_capture!(main, args);
  assert_eq!(output.trim(), "Not equal");
}
