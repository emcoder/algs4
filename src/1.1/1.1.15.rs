/* Exercise code example */
#[macro_use]
extern crate algs4;

use algs4::*;

fn histogram(a: &[i64], m: usize) -> Vec<i64> {
  let mut result: Vec<i64> = Vec::new();
  result.resize(m, 0);
  for i in a {
    if *i < m as i64 {
      result[*i as usize] += 1;
    }
  }
  result
}

fn exercise(args: Args, output: &mut Output) {
  let args: Vec<i64> = args.into_iter().map(|a| a.parse::<i64>().unwrap()).collect();
  let m = args[0];
  let result = histogram(&args[1..], m as usize);
  for n in result {
    write!(output, "{} ", n).unwrap();
  }
  writeln!(output).unwrap();
}

fn main() {
  run!(exercise);
}

#[cfg(test)]
mod tests {
  use super::exercise;

  #[test]
  fn test() {
    let args = args!("World");
    let output = run_capture!(exercise, args);
    assert_eq!(output.trim(), "Hello, World!");
  }
}
