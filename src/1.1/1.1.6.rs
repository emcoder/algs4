/* Exercise code example */
extern crate algs4;

use algs4::*;

fn exercise(_args: Args, output: &mut Output) {
  let mut f = 0;
  let mut g = 1;

  for _i in 0..=15 {
    writeln!(output, "{}", f).unwrap();
    f = f + g;
    g = f - g;
  }
}

fn main() {
  run!(exercise);
}
