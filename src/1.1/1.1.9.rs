extern crate algs4;

use algs4::*;

fn exercise(args: Args, output: &mut Output) {
  let arg: u64 = (&args[0]).parse().unwrap();
  let mut num = arg;
  let mut bin = String::from("");

  loop {
    let digit = num % 2;
    num = num.checked_div(2).unwrap();

    bin.insert_str(0, digit.to_string().as_str());

    if num <= 1 {
      if num == 1 {
        bin.insert_str(0, num.to_string().as_str());
      }
      break;
    }
  }

  writeln!(output, "Number {} in binary form is {}", arg, bin).unwrap();
}

fn main() {
  run!(exercise);
}

#[cfg(test)]
mod tests {
  use super::exercise;
  use algs4::*;

  #[test]
  fn test() {
    let output = run_capture!(exercise, args!("12345"));
    assert_eq!(
      output.trim(),
      "Number 12345 in binary form is 11000000111001"
    );
    let output = run_capture!(exercise, args!("2"));
    assert_eq!(output.trim(), "Number 2 in binary form is 10");
    let output = run_capture!(exercise, args!("777"));
    assert_eq!(output.trim(), "Number 777 in binary form is 1100001001");
    let output = run_capture!(exercise, args!("0"));
    assert_eq!(output.trim(), "Number 0 in binary form is 0");
  }
}
