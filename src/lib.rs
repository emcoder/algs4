pub use std::io::Write;
pub use utils::*;

// Create string args vector (Args) using macro that can handle string literals
#[macro_export]
macro_rules! args {
  ( $($b:expr),* ) => {{
    let mut args: Vec<String> = Vec::new();
    $(
      args.push(String::from($b));
    )*
    Args::new(args)
  }};
}

#[macro_export]
macro_rules! out {
  ( $a:expr, $($b:expr),+ ) => {{
    write!($a, $($b),+).expect("write error");
  }};
}

#[macro_export]
macro_rules! outln {
  ( $a:expr, $($b:expr),+ ) => {{
    writeln!($a, $($b),+).expect("writeln error");
  }};
}

// Run exercise
#[macro_export]
macro_rules! run {
  // Normal run exercise with system args and output to stdout
  ($f:ident) => {
    use std::io::stdout;
    use utils::args;
    $f(args(), &mut stdout());
  };

  // Run exercise with specific args and output to stdout
  ($f:ident, $a: expr) => {
    use std::io::stdout;
    $f($a, &mut stdout());
  };
}

// Run exercise with specific args collecting output to string
#[macro_export]
macro_rules! run_capture {
  ($f:ident, $a:expr) => {{
    let mut output: Vec<u8> = Vec::new();
    $f($a, &mut output);
    let output = String::from_utf8(output).expect("Failed to parse collected output");

    output
  }};
}

pub mod utils;
