// Exercise template
use algs4::*;

pub fn main(args: Args, output: &mut Output) {
  let (x, y) = (args.int(0), args.int(1));
  let u = x * y;
  outln!(output, "{}", u);
}

#[test]
fn test() {
  let args = args!("5", "7");
  let output = run_capture!(main, args);
  assert_eq!(output.trim(), "35");
}
